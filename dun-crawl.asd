;;;; dun-crawl.asd

(asdf:defsystem #:dun-crawl
  :description "Describe dun-crawl here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:random-state
	       #:for
	       #:cl-heap)
  :components ((:file "package")
	       (:file "macros")
	       (:file "util")
	       (:file "dungeon")
	       (:file "draw")
               (:file "dun-crawl")))
