;;;; dun-crawl.lisp

(in-package #:dun-crawl)

(defparameter *debug-generation* T)

(defparameter *wall-symbol* "█")
(defparameter *room-symbol* " ")
(defparameter *hall-symbol* " ")


(defclass settings ()
  ((height
    :initarg :height
    :accessor height)
   (width
    :initarg :width
    :accessor width)
   (max-hardness
    :initarg :max-hardness
    :accessor max-hardness)
   (min-room-h
    :initarg :min-room-h
    :accessor min-room-h)
   (min-room-w
    :initarg :min-room-w
    :accessor min-room-w)))

(defun gen-basic-settings (&key (height 20) (width 40) (hard 10) (min-r-h 3) (min-r-w 4))
    (make-instance 'settings :height height
     :width width
     :max-hardness hard
     :min-room-h min-r-h
     :min-room-w min-r-w))

(defun setup ()
  (progn
    (seed-rand)
    (gen-basic-settings)))

(defun start ()
  ;; Set up
  (let* ((game-settings (setup))
	 (dungeon (gen-dungeon game-settings)))
    (draw-whole-dungeon dungeon)
    ;; Game loop (when drawing is ready for it)
    ))

(defun start-big ()
   (let* ((game-settings (progn (seed-rand 6000) (gen-basic-settings :height 50 :width 150 :min-r-h 10 :min-r-w 20)))
	 (dungeon (gen-dungeon game-settings)))
    (draw-whole-dungeon dungeon)
    ;; Game loop (when drawing is ready for it)
    ))
