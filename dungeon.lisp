
(in-package #:dun-crawl)

(defclass dungeon ()
  ((level
    :initarg :level
    :accessor level)
   (rooms
    :initform NIL
    :accessor rooms)
   (max-x
    :initarg NIL
    :accessor max-x)
   (max-y
    :initarg NIL
    :accessor max-y)))

(defun fill-level (settings)
  (let ((level NIL))
    (for:for ((rows repeat (height settings)))
      (let ((curr-row NIL))
	(for:for ((cols repeat (width settings)))
	  (push (rand-int (max-hardness settings)) curr-row))
	(push curr-row level)))
    level))

(defun gen-room (settings)
  (list :x (rand-int (width settings))
	:y (rand-int (height settings))
	:h (+ 2 (rand-int (min-room-h settings)))
	:w (+ 4 (rand-int (min-room-w settings)))))

(defun room-fits (new-room frame)
  (let* ((x (getf new-room :x))
	 (y (getf new-room :y))
	 (h (getf new-room :h))
	 (w (getf new-room :w))
	 (not-to-high (< (+ y h) (- (max-row frame) 1)))
	 (not-to-wide (< (+ x w) (- (max-col frame) 1)))
	 (not-to-low (> x 0))
	 (not-to-left (> y 0))
	 (no-overlap
	  (let ((overlap)) NIL (for:for ((curr-x ranging (- x 1) (+ x w 1)))
				 (for:for ((curr-y ranging (- y 1) (+ y h 1)))
				   (setq overlap (or overlap  (get-point frame curr-x curr-y)))))
	       (not overlap))))
    (and not-to-high not-to-wide no-overlap not-to-low not-to-left)))

(defun apply-rooms-to-dun (dun rooms-list)
  (for:for ((room over rooms-list))
    (let* ((x (getf room :x))
	   (y (getf room :y))
	   (h (getf room :h))
	   (w (getf room :w))
	   (x-upper (+ x w))
	   (y-upper (+ y h))
	   (l-frame (level dun)))
      (for:for ((curr-x ranging x x-upper))
	(for:for ((curr-y ranging y y-upper))
	  (set-point l-frame curr-x curr-y (make-instance 'tile :hardness 0 :t-type 'ROOM)))))))

(defun add-rooms (dun settings)
  (let ((max-attempts 1000)
	(rooms-list NIL)
	(room-frame (gen-frame (max-y dun) (max-x dun))))
    (for:for ((i repeat max-attempts))
      (let* ((new-room (gen-room settings))
	     (x (getf new-room :x))
	     (y (getf new-room :y))
	     (h (getf new-room :h))
	     (w (getf new-room :w)))
	(if (room-fits new-room room-frame)
	    (progn
	      (push new-room rooms-list)
	      (let ((x-upper (+ x w))
		    (y-upper (+ y h)))
		(for:for ((curr-x ranging x x-upper))
		  (for:for ((curr-y ranging y y-upper))
		    (set-point room-frame curr-x curr-y T)))
		)))))
    (progn
      (setf (rooms dun) rooms-list)
      (apply-rooms-to-dun dun rooms-list)
      )))

(defun pythag (x y)
  (round (sqrt (+ (* (abs x) (abs x)) (* (abs y) (abs y))))))

(defun gen-dist-map (start-x start-y target-x target-y in-frame)
  (let ((distance-frame (gen-frame (max-row in-frame) (max-col in-frame) (lambda () 1000000)))
	(to-visit (make-instance 'cl-heap:priority-queue))
	(visited NIL)
	(curr-point NIL)
	(curr-dist NIL)
	(up-pos NIL)
	(down-pos NIL)
	(left-pos NIL)
	(right-pos NIL)
	(h-fun (lambda (coord dist) (+ dist (pythag (- target-x (x coord)) (- target-y (y coord)))))))
    (progn
      (set-point distance-frame start-x start-y 0)
      (cl-heap:enqueue to-visit (make-coord start-x start-y) 0)
      (push (format NIL "~a,~a" start-x start-y) visited)
      (loop while (and (cl-heap:peep-at-queue to-visit) (not (coord-eq (cl-heap:peep-at-queue to-visit) (make-coord target-x target-y))))
	 do (progn
	      
	      (setq curr-point (cl-heap:dequeue to-visit))
	      (setq curr-dist (get-point distance-frame (x curr-point) (y curr-point)))
	      (setq up-pos    (make-checked-coord (x curr-point) (1- (y curr-point)) (max-col in-frame) (max-row in-frame)))
	      (setq down-pos  (make-checked-coord (x curr-point) (1+ (y curr-point)) (max-col in-frame) (max-row in-frame)))
	      (setq left-pos  (make-checked-coord (1- (x curr-point)) (y curr-point) (max-col in-frame) (max-row in-frame)))
	      (setq right-pos (make-checked-coord (1+ (x curr-point)) (y curr-point) (max-col in-frame) (max-row in-frame)))
	      (if up-pos
		  (setq visited (work-on-cell h-fun curr-dist up-pos    in-frame distance-frame visited to-visit)))
	      (if down-pos 
		  (setq visited (work-on-cell h-fun curr-dist down-pos  in-frame distance-frame visited to-visit)))
	      (if left-pos
		  (setq visited (work-on-cell h-fun curr-dist left-pos  in-frame distance-frame visited to-visit)))
	      (if right-pos
		  (setq visited (work-on-cell h-fun curr-dist right-pos in-frame distance-frame visited to-visit))))))
    (if *debug-generation*
	(progn
	  (format T "~%~%Path from x: ~a y: ~a  =====>>>> x: ~a y: ~a~%~%" start-x start-y target-x target-y)
	  (print "current open space in dungeon:")
	  (draw-floor-frame in-frame)
	  (format T "~%Distance Map:~%")
	  (draw-dist-map distance-frame)))
    distance-frame))

(defun work-on-cell (h-fun dist curr-loc floor-frame dist-frame visited visit-queue)
  (progn
    (let ((new-dist (+ dist 1 (hardness (get-point floor-frame (x curr-loc) (y curr-loc)))))
	  (curr-dist (get-point dist-frame (x curr-loc) (y curr-loc))))
      (if (< new-dist curr-dist)
	  (set-point dist-frame (x curr-loc) (y curr-loc) new-dist)))
    (if (not (member (format NIL "~a,~a" (x curr-loc) (y curr-loc)) visited :test #'string=))
	(progn
	  (push (format NIL "~a,~a" (x curr-loc) (y curr-loc)) visited)
	  (cl-heap:enqueue visit-queue curr-loc (funcall h-fun curr-loc (get-point dist-frame (x curr-loc) (y curr-loc))))))
    visited))


(defun make-hall-tile (f-frame coord)
  (let ((tile (get-coord f-frame coord)))
    (if (not (eq (t-type tile) 'ROOM))
	(set-coord f-frame coord (make-instance 'tile :hardness 0 :t-type 'HALL)))))

(defun find-next-coord (dist-map coord)
  (let ((queue (make-instance 'cl-heap:priority-queue))
	(up    (make-coord (x coord) (1- (y coord))))
	(down  (make-coord (x coord) (1+ (y coord))))
	(left  (make-coord (1- (x coord)) (y coord)))
	(right (make-coord (1+ (x coord)) (y coord))))
    (progn
      (cl-heap:enqueue queue up (get-coord dist-map up))
      (cl-heap:enqueue queue down (get-coord dist-map down))
      (cl-heap:enqueue queue left (get-coord dist-map left))
      (cl-heap:enqueue queue right (get-coord dist-map right))
      (cl-heap:dequeue queue))))

(defun gen-path-between-points (start-x start-y end-x end-y floor-frame)
  (let ((dist-map (gen-dist-map start-x start-y end-x end-y floor-frame))
	(curr-c (make-coord end-x end-y)))
    (progn
      (do-while (AND (not (coord-eq curr-c (make-coord start-x start-y))))
	(progn
	  (make-hall-tile floor-frame curr-c)
	  (setq curr-c (find-next-coord dist-map curr-c)))))))


(defun link-rooms (r1 r2 floor-frame)
  (let* ((r1-x (getf r1 :x))
	 (r1-y (getf r1 :y))
	 (r2-x (getf r2 :x))
	 (r2-y (getf r2 :y))
	 (hall-path (gen-path-between-points r1-x r1-y r2-x r2-y floor-frame)))
    (for:for ((coord over hall-path))
      (let ((dun-point (get-point floor-frame (x coord) (y coord))))
	(progn
	  (set-hardness dun-point 0)
	  (if (eq (t-type dun-point) 'WALL)
	      (set-t-type dun-point 'HALL)))))))

(defun add-halls (dun)
  (let* ((rooms (rooms dun))
	 (curr-room (car rooms)))
    (for:for ((next-room over (cdr rooms)))
      (progn
	(link-rooms curr-room next-room (level dun))
	(setq curr-room next-room)))))

(defun gen-dungeon (&optional (settings (setup)))

  (let ((new-dun (make-instance 'dungeon
				:level (gen-frame (height settings) (width settings)
						  (lambda () (make-instance 'tile :hardness (rand-int (max-hardness settings) 1) :t-type 'WALL))))))
    (progn
      (setf (max-y new-dun) (- (height settings) 1))
      (setf (max-x new-dun) (- (width settings) 1))
      (add-rooms new-dun settings)
      (if *debug-generation* (draw-whole-dungeon new-dun))
      (add-halls new-dun)
      new-dun)))

(defmethod get-tile ((dun dungeon) x y)
  (nth y (nth x (level dun))))

(defmethod draw-dun ((dun dungeon))
  (for:for ((row over (data (level dun))))
    (for:for ((col over row))
      (format T "~a" col))
    (format T "~%")))

(defmethod get-tile ((dun dungeon) x y)
  (nth y (nth x (level dun))))

(defmethod draw-dun ((dun dungeon))
  (for:for ((row over (level dun)))
    (for:for ((col over row))
      (format T "~a" col))
    (format T "~%")))
