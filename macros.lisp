(in-package #:dun-crawl)

(defmacro do-while (condition &rest body)
  `(loop while ,condition
       do ,@body))
