(in-package #:dun-crawl)

(defparameter *seeded-gen* NIL)

(defun seed-rand (&optional (seed 1234))
  (setq *seeded-gen* (random-state:make-generator :mersenne-twister-32 seed)))

(defun rand-int (max &optional (min 0))
  (random-state:random-int *seeded-gen* min max))

(defclass coord ()
  ((x
    :initform 0
    :initarg :x
    :accessor x)
   (y
    :initform 0
    :initarg :y
    :accessor y)))

(defun make-checked-coord (x y ceil-x ceil-y)
  (if (and (> x 0) (> y 0) (< x (1- ceil-x)) (< y (1- ceil-y)))
      (make-coord x y)
      NIL))

(defun make-coord (x y)
  (make-instance 'coord :x x :y y))

(defmethod coord-eq ((this coord) that)
  (AND (= (x this) (x that)) (= (y this) (y that))))

(defclass tile ()
  ((hardness
    :initarg :hardness
    :accessor hardness)
   (t-type
    :initarg :t-type
    :accessor t-type)))

(defmethod set-hardness ((obj tile) new-hardness)
    (setf (hardness obj) new-hardness))

(defmethod set-t-type ((obj tile) new-t-type)
  (setf (t-type obj) new-t-type))


(defclass frame ()
  ((data
    :initarg :data
    :accessor data)
   (max-col
    :initarg :max-col
    :accessor max-col)
   (max-row
    :initarg :max-row
    :accessor max-row)
   (last-loc
    :initform (list :x 0 :y 0)
    :accessor last-loc)))

(defmethod get-coord ((f frame) (c coord))
  (get-point f (x c) (y c)))


(defmethod get-point ((f frame) x y)
  (if (or (< x 0) (< y 0) (>= x (max-col f)) (>= y (max-row f)))
      NIL
      (nth x (nth y (data f)))))


(defmethod set-coord ((f frame) (c coord) val)
  (set-point f (x c) (y c) val))


(defmethod set-point ((f frame) x y val)
  (if (not (or (>= x (max-col f)) (>= y (max-row f))))
      (setf (nth x (nth y (data f))) val)))

(defun gen-frame (row-count col-count &optional (push-fun (lambda () NIL)))
  (let ((frame NIL))
    (for:for ((row repeat row-count))
      (let ((curr-row NIL))
	(for:for ((col repeat col-count))
	  (push (funcall push-fun) curr-row))
	(push curr-row frame)))
    (make-instance 'frame :data frame :max-col col-count :max-row row-count)))

(defmethod paint-nils-frame ((f frame))
  (progn
    (format T "~%")
    (for:for ((i repeat (length (nth 0 (data f)))))
      (format T " ~a" (mod (- i 1) 10)))
    (format T "~%")
    (for:for ((i repeat (+ 1 (* 2 (length (nth 0 (data f)))))))
      (format T "-"))
    (format T "~%")
    (for:for ((row over (data f)))
      (progn
	(format T "|")
	(for:for ((col over row))
	  (format T "~a|" (if col
			     0
			     " ")))
	(format T "~%")))
    (for:for ((i repeat (+ 1 (* 2 (length (nth 0 (data f)))))))
      (format T "-"))))
